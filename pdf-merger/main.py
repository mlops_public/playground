import argparse
import PyPDF2
import os
from typing import List

def merge_pdfs(input_folder: str, output_file: str) -> None:
    """
    Merge multiple PDF files into a single PDF.

    Args:
        input_folder (str): The path to the folder containing PDF files to be merged.
        output_file (str): The path to the output file where the merged PDF will be saved.

    Returns:
        None
    """
    pdf_files = [f for f in os.listdir(input_folder) if f.endswith('.pdf') or f.endswith('.PDF')]
    pdf_files.sort()

    pdf_merger = PyPDF2.PdfMerger()

    for pdf_file in pdf_files:
        pdf_path = os.path.join(input_folder, pdf_file)
        pdf_merger.append(pdf_path)

    with open(output_file, 'wb') as merged_pdf:
        pdf_merger.write(merged_pdf)
        

def extract_pages_from_pdf(input_file: str, output_file: str, start_page: int, end_page: int) -> None:
    """
    Extract a range of pages from a PDF.

    Args:
        input_file (str): The path to the input PDF file.
        output_file (str): The path to the output PDF file where the extracted pages will be saved.
        start_page (int): The starting page number for extraction.
        end_page (int): The ending page number for extraction.

    Returns:
        None
    """
    with open(input_file, 'rb') as pdf_file:
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        pdf_writer = PyPDF2.PdfWriter()

        for page_num in range(start_page - 1, end_page):  # Adjust to 0-based index
            pdf_writer.add_page(pdf_reader.pages[page_num])

        with open(output_file, 'wb') as output_pdf:
            pdf_writer.write(output_pdf)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Merge PDF files or extract pages from a PDF.')
    parser.add_argument('--input_folder', type=str, help='Path to the folder containing PDF files to be merged.')
    parser.add_argument('--output_file', type=str, help='Path to the output file where the result will be saved.')
    parser.add_argument('--start_page', type=int, help='Start page for extraction.')
    parser.add_argument('--end_page', type=int, help='End page for extraction.')
    parser.add_argument('--extract_pages', action='store_true', help='Flag to indicate whether to extract pages.')

    args = parser.parse_args()

    if args.extract_pages:
        extract_pages_from_pdf(args.input_folder, args.output_file, args.start_page, args.end_page)
        print(f'Pages extracted and saved to {args.output_file}')
    else:
        merge_pdfs(args.input_folder, args.output_file)
        print(f'Merged PDFs saved to {args.output_file}')
